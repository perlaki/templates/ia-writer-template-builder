const path = require('path');
const gulp = require('gulp');
const sass = require('gulp-sass');
const peditor = require('gulp-plist');

const YAML = require('yamljs');

const info = YAML.load('src/Info.yaml');
const outPath = `${info.CFBundleName}.iatemplate/Contents`;
const resourcePath = path.join(outPath, 'Resources');

gulp.task('info', function () {
    gulp.src('Info.plist')
        .pipe(peditor(info))
        .pipe(gulp.dest(outPath));
});

gulp.task('files', function () {
    return gulp.src('src/Resources/*.{html,css,js,svg}')
        .pipe(gulp.dest(resourcePath));
});

gulp.task('css', function () {
    return gulp.src('src/Resources/*.scss')
        .pipe(sass())
        .pipe(gulp.dest(resourcePath));
});

gulp.task('default', ['files', 'css', 'info']);